#!/usr/bin/env python3
import unittest

import project as p
import dice_roller as dr


def main() -> None:
    test_get_roller()
    TestMockRandom.test_generate_results()
    test_print_dice_results()


def test_get_roller() -> None:
    ...


class TestMockRandom(unittest.TestCase):
    def test_generate_results(self) -> None:
        ...


def test_print_dice_results() -> None:
    ...
