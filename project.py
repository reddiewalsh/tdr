#!/usr/bin/env python3

import sys


from parser import parse_arguments
import dice_roller as d


def main() -> None:
    args = parse_arguments()
    if args.interactive:
        roller: d._Roller | None = None
        while True:
            action = input("(r)eroll (q)uit\nEnter Dice: ")
            if action.casefold().strip() == "q":
                sys.exit()
            elif action.casefold().strip() == "r" or action.casefold().strip() == "":
                if roller:
                    roller.roll()
                    print_dice_results(roller)
            else:
                roller = get_roller(action)
                if roller:
                    generate_results(roller)
                    print_dice_results(roller)

    elif args.dice:
        roller = get_roller(args.dice)
        if roller:
            generate_results(roller)
            print_dice_results(roller)
        sys.exit()


def generate_results(roller: d._Roller) -> None:
    roller.parse_notation()
    roller.roll()


def get_roller(dice_notation: str) -> d._Roller | None:
    if d.STANDARD_DICE_NOTATION.match(dice_notation):
        return d.StandardRoller(dice_notation)
    elif d.FATE_DICE_NOTATION.match(dice_notation):
        return d.FateRoller(dice_notation)
    else:
        return None


def print_dice_results(roller_object: d._Roller) -> None:
    print("-" * 10)
    print(f"{roller_object.input} rolls ... {roller_object.results}")
    print(f"TOTAL: {roller_object.total}")
    print("-" * 10)


if __name__ == "__main__":
    main()
