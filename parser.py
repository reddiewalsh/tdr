#!/usr/bin/env python3

import argparse
from argparse import Namespace


def parse_arguments() -> Namespace:
    parser = argparse.ArgumentParser(
        prog="CS50p: Final Project",
        description="A python program that takes a user-inputted string in 'dice notation' and outputs a resulting die roll.",
    )
    # Should hold one arg for now, but possible add more later
    parser.add_argument(
        "dice",
        action="store",
        nargs="?",
        help="enter dice using dice notation",
    )
    parser.add_argument(
        "-i",
        "--interactive",
        default=False,
        action="store_true",
        help="Enter interactive dice rolling.",
    )
    return parser.parse_args()


if __name__ == "__main__":
    pass
