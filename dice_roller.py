#!/usr/bin/env python3

from abc import ABC, abstractmethod
import random
import re
from re import Pattern

# This is completely untested and made from my memory of how regex works,
# so... definitely broken.
STANDARD_DICE_NOTATION = re.compile(
    r"(?P<num_dice>\d*)d(?P<dice_val>100|20|12|10|8|6|4|2)(?P<mod>[+|-]\d+)?"
)
FATE_DICE_NOTATION = re.compile(r"(?P<num_dice>\d*)d[f,F](?P<mod>[+|-]\d+)?")


class _Roller(ABC):
    def __init__(self, input: str):
        self.input = input
        self._num_dice = 1
        self._modifier = 0

    @property
    def input(self) -> str:
        return self._input

    @input.setter
    def input(self, dice_notation: str) -> None:
        self._input = dice_notation
        self.parse_notation()

    @abstractmethod
    def parse_notation(self) -> None:
        """Parse the input string to set attributes"""
        try:
            values = STANDARD_DICE_NOTATION.match(self.input)
            assert values is not None
            self._dice_val = int(values.group("dice_val"))
            if values.group("num_dice"):
                self._num_dice = int(values.group("num_dice"))
            if values.group("mod"):
                self._modifier = int(values.group("mod").strip("+"))
        except AssertionError:
            raise NotationError(f"{self.input} does not match STANDARD_DICE_NOTATION")

    @property
    def num_dice(self) -> int:
        return self._num_dice

    @property
    def modifier(self) -> int:
        return self._modifier

    @abstractmethod
    def roll(self):
        """The roll method will generate a list of random numbers to represent the roll of the dice.
        The results of these random numbers are set to the attribute results and combined to make a total
        """
        self._results = (0,)
        self._total = sum(self.results, self.modifier)

    @property
    def results(self) -> tuple[int]:
        return self._results

    @property
    def total(self) -> int:
        return self._total


class StandardRoller(_Roller):
    def parse_notation(self) -> None:
        return super().parse_notation()

    @property
    def dice_val(self) -> int:
        return self._dice_val

    def roll(self) -> None:
        results = []
        for _ in range(self.num_dice):
            die = random.randint(1, self.dice_val)
            results.append(die)
        self._results = tuple(results)
        self._total = sum(results, self.modifier)


class FateRoller(_Roller):
    def __init__(self, input: str):
        self.input = input
        self._num_dice = 4
        self._modifier = 0

    def parse_notation(self) -> None:
        """Parse the input string to set attributes"""
        try:
            values = FATE_DICE_NOTATION.match(self.input)
            assert values is not None
            if values.group("num_dice"):
                self._num_dice = int(values.group("num_dice"))
            if values.group("mod"):
                self._modifier = int(values.group("mod").strip("+"))
        except AssertionError:
            raise NotationError(f"{self.input} does not match FATE_DICE_NOTATION")

    def roll(self) -> None:
        results: list[int] = []
        for _ in range(self.num_dice):
            die = random.randint(-1, 1)
            results.append(die)
        self._results = tuple(results)
        self._total = sum(results, self.modifier)


class NotationError(ValueError): ...


if __name__ == "__main__":
    pass
